import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTransistorAmount(): Promise<string> {
    return element(by.css('app-root #data-pane .large.display')).getText() as Promise<string>;
  }

  clickRoundStart(): Promise<void> {
    return element(by.id('round-start-button')).click() as Promise<void>;
  }

  clickSpeedState(): Promise<void> {
    return element(by.id('increase-speed-button')).click() as Promise<void>;
  }

  clickReplicationState(): Promise<void> {
    return element(by.id('increase-replication-button')).click() as Promise<void>;
  }

  clickCapState(): Promise<void> {
    return element(by.id('increase-cap-button')).click() as Promise<void>;
  }
}
