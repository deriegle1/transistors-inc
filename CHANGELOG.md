# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.1-alpha.7](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.6...v0.0.1-alpha.7) (2020-06-02)


### Features

* **ai.component:** adds initial cards for ai components ([851ab66](https://gitlab.com/HeinousTugboat/transistors-inc/commit/851ab66b30dce6a78606ba96206fabf5579fcc35))
* **ai.service:** adds ability to progress AI generation via the power of the mouse ([126d4fd](https://gitlab.com/HeinousTugboat/transistors-inc/commit/126d4fd3fef8e472940bd711219f8e5da4c8d455)), closes [#30](https://gitlab.com/HeinousTugboat/transistors-inc/issues/30)
* **ais.component:** adds initial ai.component ([64040ae](https://gitlab.com/HeinousTugboat/transistors-inc/commit/64040ae5376c9f7fffc5b1eec257f487b9b1aff7))
* **log:** adds error log entries ([f97d505](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f97d50564c7e8e1da45beadedb625da3986f1384))
* **upgrades.service:** overhauls upgrades.component, adds compute upgrade buttons ([0da67d0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0da67d0d81753fddaca9d078bf499b71cf184c95))
* moves reset button from diagnostics to options ([96ce17e](https://gitlab.com/HeinousTugboat/transistors-inc/commit/96ce17e53843a5148878a63e57938f457cc8a68e))
* updates state buttons to grow to fill width of column ([7666b53](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7666b534b5318f3590ebbec56bf92d393dd9f906))


### Bug Fixes

* **upgrades.data:** corrects generators for m and t compute upgrades ([1e39168](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1e39168229b19051facc47f1e65f3d061f084418))
* **upgrades.service:** updates how compute upgrades get reset to be more correct ([709e3f4](https://gitlab.com/HeinousTugboat/transistors-inc/commit/709e3f44ef593fa3f7cffa205d12d953a370b072))

### [0.0.1-alpha.6](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.5...v0.0.1-alpha.6) (2020-06-01)


### Features

* **diagnostics.component:** adds a big red button ([1ae8556](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1ae855656b348f90f22aace5df4f80deed774599))
* adds initial slider component and options page ([7cda978](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7cda97856db194303577f67360af9059940b95b9))
* adds new sell buttons ([2cb7365](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2cb736504c58ba7f143abed3724d0c99f47548da))
* adds progress bar for AI generation ([d795dab](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d795dab6419771dd081d43adaf2ef74e2d5f8c0c)), closes [#15](https://gitlab.com/HeinousTugboat/transistors-inc/issues/15)
* **ai.service:** adds f calculation ([59a893a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/59a893a8320e1e34a230ce8222749dcabe40b03b))
* **ai.service:** rounds out upgrades and modifiers for AI and Upgrades ([61a06c1](https://gitlab.com/HeinousTugboat/transistors-inc/commit/61a06c17a6ebd29f2f618e703f831283cbcd4852))
* **ai.service:** updates so all AI tiers get 20% chance of special line 3 ([f6b5d3a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f6b5d3add3511969b6ea6bad9cc72fcae4310357))
* **transistors.service:** incorporates AI mod updates, rewrites Compute equation .. again ([cd87ed1](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cd87ed1e04473c698c61bf240b192f40c12ca2e9))
* **upgrades.service:** add compute upgrades, updates credit upgrades ([e942f14](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e942f1493d40b68ab952e69d0fc5f62df0d1c69f)), closes [#8](https://gitlab.com/HeinousTugboat/transistors-inc/issues/8)
* **upgrades.service:** adds dynamic ids to upgrades, rehydrates using dynamic ids ([d18f9de](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d18f9deb8f339405f2bdd9e189356b7e458ffb5f))


### Bug Fixes

* **ai.service:** corrects nobel AI calculating lines incorrectly ([645b2dc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/645b2dc59ad24c1851f9981f8e9ada1b5aac25f2))
* **log.component:** changes history to slice from end instead of start ([d0d205a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d0d205aa873b86a0f61bb0e65d23240e87e37af3))
* **transistors.service:** updates sell code to properly sell accumulated transistors ([a7972d5](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a7972d59b063322bed87946cf8982960f28e46b7))
* **upgrades.service:** adds recalc to resetCompute.. oops ([5d403a2](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5d403a2fb589c3af04b890f1dc97e6d2063d1df3))
* **upgrades.service:** removes purchased upgrades from available upgrades on rehydration ([90edd32](https://gitlab.com/HeinousTugboat/transistors-inc/commit/90edd32c610ddd2a732ed8ffbfadffb78f732cae))

### [0.0.1-alpha.5](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.3...v0.0.1-alpha.5) (2020-05-30)


### ⚠ BREAKING CHANGES

* **ai.service:** AI has a different interface now
* numbers are numbers no longer

### Features

* **ai.service:** completely overhauls AI system and classes ([fea712c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/fea712cd8797d6551f1e9b7f6d0f010bdf28103e)), closes [#16](https://gitlab.com/HeinousTugboat/transistors-inc/issues/16)
* **app.component:** refactors round timer to use new progress bar component ([e0ffd8f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e0ffd8fdcde0f37358d2e6513db91a66107316d5))
* **currency.service:** adds currency service and existing currencies (and tests! wow!) ([d72d9e7](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d72d9e718434dcf9d1bed95f35c0c5caa92bce98))
* **currency.service:** updates all components/services to utilize currency service ([8d42eb2](https://gitlab.com/HeinousTugboat/transistors-inc/commit/8d42eb2ddb7f3383d0ad38de0478a1d01a3a0da7)), closes [#12](https://gitlab.com/HeinousTugboat/transistors-inc/issues/12) [#3](https://gitlab.com/HeinousTugboat/transistors-inc/issues/3)
* **diagnostics.component:** adds button to add 10k accumulated transistors ([cff1a40](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cff1a400e325126524a88da74227f0b4c7819339))
* **diagnostics.component:** splits diagnostics into separate floating component ([13d68bb](https://gitlab.com/HeinousTugboat/transistors-inc/commit/13d68bb574679505880af64d874e0351c822e8ad))
* **log.component:** reverses event log order, fades items so only 15 most recent are visible ([c31598c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c31598c26bd434a02624b973560d1140dcce72d5))
* **log.components:** adds logLength to options service, takes slice of log instead of full thing ([aeeda2c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/aeeda2ca52d4e8c20f3ae1dd86888c166e573430))
* **progress.component:** adds progress component to show dynamic width bars ([ad37912](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ad379126919c4b5ee97c5eacde9bdca719fe0206))
* **random.service:** adds initial pass and RNG service, adds diagnostics to check results ([b37dda6](https://gitlab.com/HeinousTugboat/transistors-inc/commit/b37dda66c1cd0febfb99a211c4d533c47a27decc))
* **romu-random:** adds fastrange to DuoJr ([eb6cc40](https://gitlab.com/HeinousTugboat/transistors-inc/commit/eb6cc40406ba62de04ddf0c83d5966eab826f9f5))
* **romu-random:** adds init function to DuoJr ([a7de27f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a7de27f20b18c9ea37216f1b09ddf16da799728f))
* **romu-random:** adds RNG type for the one we're actually using ([ed568ec](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ed568ecccb4c016e6a66f4f81e4381da73189c78))
* **romu-random:** updates PRNG interfaces to use better names ([a792c02](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a792c022a2490bdacfd5b6ea178a641b18940af0))
* adds romu-random algorithms and tests, adds BigInt support ([a9fccf4](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a9fccf411295583122cc69175bb302a79b706332))
* enables strict mode, updates code to follow strict mode restrictions ([497c2a6](https://gitlab.com/HeinousTugboat/transistors-inc/commit/497c2a642617c0eba04efcc7883798d165a4105d))
* replaces number with break_infinity's Decimal ([d5eb42e](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d5eb42e131b24284138c7c64281dd9bfc0f8056e)), closes [#5](https://gitlab.com/HeinousTugboat/transistors-inc/issues/5)
* updates page title to "Transistors Inc" ([e96bb7b](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e96bb7b3dff3d7cc9547286bf37bb02d6552192d))
* **transistors.service:** adds minor tweaks to accumulated transistors and display ([21f1d86](https://gitlab.com/HeinousTugboat/transistors-inc/commit/21f1d86e10c6ab14309834735946990c68aa4a27))
* **transistors.service:** moves initial transistor to seed transistor, updates acc. transistors ([ab638fc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ab638fcad40af69ba1675bf8dcb1fd9147c48fc2)), closes [#7](https://gitlab.com/HeinousTugboat/transistors-inc/issues/7)
* **transistors.service:** updates default INCREASE_R state to 1.5x instead of 2x ([2b555d0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2b555d08a35ed7b737444883e083781d387bdbc2))


### Bug Fixes

* **ai.service:** fixes importing findTier from wrong place ([307cbb8](https://gitlab.com/HeinousTugboat/transistors-inc/commit/307cbb821b0ee12376ed39d44a766d49237711b4))
* **e2e:** updates e2e test to reflect 0.0 transistor on load ([e5e219a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e5e219a341d4239657c5cb2d9755a69173e54945))
* **romu-random:** updates flaky test that only checks 100 values vs 100k ([ed79b3f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ed79b3f08b86a2bfaf99ae215358e6d0127e88b9))
* **transistors.service:** fixes compute calculation so it should never drop negative ([3ccd7aa](https://gitlab.com/HeinousTugboat/transistors-inc/commit/3ccd7aa2e177ec865c4adddd094b05de8fecb58f)), closes [#17](https://gitlab.com/HeinousTugboat/transistors-inc/issues/17)
* **units.pipe:** updates units pipe to be polymorphic for decimal and number ([55a1a4f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/55a1a4f899d1b4fdfbfc50f17cca6488bdf58ebf))
* moved break_infinity to vendor folder, added declarations file from repo ([0b1fc1c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0b1fc1c04a1922b7abd227dad08c0d2d286936b9))
* **transistors.service:** updates m to reset at the beginning of each round ([6832afb](https://gitlab.com/HeinousTugboat/transistors-inc/commit/6832afbe8917f8ecffff25a68fff0e5a546a6081)), closes [#10](https://gitlab.com/HeinousTugboat/transistors-inc/issues/10)

### [0.0.1-alpha.4](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.3...v0.0.1-alpha.4) (2020-05-27)


### Features

* **app.component:** refactors round timer to use new progress bar component ([e0ffd8f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e0ffd8fdcde0f37358d2e6513db91a66107316d5))
* **diagnostics.component:** splits diagnostics into separate floating component ([13d68bb](https://gitlab.com/HeinousTugboat/transistors-inc/commit/13d68bb574679505880af64d874e0351c822e8ad))
* **log.component:** reverses event log order, fades items so only 15 most recent are visible ([c31598c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c31598c26bd434a02624b973560d1140dcce72d5))
* **progress.component:** adds progress component to show dynamic width bars ([ad37912](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ad379126919c4b5ee97c5eacde9bdca719fe0206))
* **transistors.service:** adds minor tweaks to accumulated transistors and display ([21f1d86](https://gitlab.com/HeinousTugboat/transistors-inc/commit/21f1d86e10c6ab14309834735946990c68aa4a27))
* **transistors.service:** moves initial transistor to seed transistor, updates acc. transistors ([ab638fc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ab638fcad40af69ba1675bf8dcb1fd9147c48fc2)), closes [#7](https://gitlab.com/HeinousTugboat/transistors-inc/issues/7)
* **transistors.service:** updates default INCREASE_R state to 1.5x instead of 2x ([2b555d0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2b555d08a35ed7b737444883e083781d387bdbc2))


### Bug Fixes

* **e2e:** updates e2e test to reflect 0.0 transistor on load ([e5e219a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e5e219a341d4239657c5cb2d9755a69173e54945))
* **transistors.service:** updates m to reset at the beginning of each round ([6832afb](https://gitlab.com/HeinousTugboat/transistors-inc/commit/6832afbe8917f8ecffff25a68fff0e5a546a6081)), closes [#10](https://gitlab.com/HeinousTugboat/transistors-inc/issues/10)

### [0.0.1-alpha.3](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.2...v0.0.1-alpha.3) (2020-05-26)


### Features

* **app.component:** moved train/sell buttons to be side by side ([9b89571](https://gitlab.com/HeinousTugboat/transistors-inc/commit/9b89571278f3d29ed109a7cf8f9bdaf8af648e6e))
* **app.component:** rerannges state buttons, changes default state ([a64bc5f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a64bc5f0aefbede421f1ed4fa92a3f2ea63d575e))
* **transistors.service:** adds accumulated transistors to track total earned ([7d997dd](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7d997dd06e383278aba8beed6fe186271b76b486)), closes [#2](https://gitlab.com/HeinousTugboat/transistors-inc/issues/2)
* **transistors.service:** reduces default r to where the first round only gets to about 1.2 ([2ef605a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2ef605a13372df76c3fd995b2c10bfa94ec5fb3c))
* **transistors.service:** updates compute formula to match transistor gain formula ([a5b5f36](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a5b5f369f1c3f885f58bcd1ead942b4168bd767b))
* **transistors.service:** updates formula to use stable continously compounding formula ([f825845](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f825845eef210ebdcb6de4ed5f9518661ca17b78))
* **transistors.service:** updates sell logic and refactors trainedAI to consumedTransisors ([1b28dec](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1b28decfe540bd0d0bbfda2115edb136bb447384))
* **transistors.service:** updates to autosell transistors if starting a round without using them ([ae11c26](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ae11c26d8661f78e207fdff3e3b4bb81edf8e059))
* **upgrades.service:** adds initial implementation pass for upgrades ([e25a342](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e25a3429db97962f694e9358ede6874b64089fba))
* **utils:** adds assert-never ([e40c0a1](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e40c0a117a91f23b4a69be22a2b07f246625d97f))
* **utils:** adds clamp function with tests ([39e6c85](https://gitlab.com/HeinousTugboat/transistors-inc/commit/39e6c8524186c16f2a5a38bd21e8a5c3d6679bb2))

### [0.0.1-alpha.2](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.1...v0.0.1-alpha.2) (2020-05-25)


### Features

* **ai:** adds ability to train AIs, rounds out code for building AIs ([7ac141d](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7ac141d65b8c5c13af1fa149741661f022b91b3c))
* **ais.component:** adds display for in progress AI ([4ebcff7](https://gitlab.com/HeinousTugboat/transistors-inc/commit/4ebcff7286ba3ca14c479764fdd431c18ba40c23))
* adds booleans to transistors and ai services ([a660fc0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a660fc0ecce94e039b6d6552f6c5ab0676b12517))
* **ai:** refactors ai services and components into folder ([dd937b9](https://gitlab.com/HeinousTugboat/transistors-inc/commit/dd937b9f9092afdd33fd32ea943f1fe7acc28552))
* **log.service:** adds log and logger service ([d8b2624](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d8b2624c0522341d4ab4d7ea61fabd62cdba757b))
* **not-found.component:** adds not-found component and path ([89cd0b7](https://gitlab.com/HeinousTugboat/transistors-inc/commit/89cd0b7af8ccf2f4a6531f098439b0ea399dabf1))
* **upgrade:** adds initial upgrade service and upgrades component ([d970ae3](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d970ae300cfd1f70ad3ec3d97ec8f5a4d5e1883c))
* adds changelog view! ([64a6447](https://gitlab.com/HeinousTugboat/transistors-inc/commit/64a6447e65fc624921a36b039733848fe906328e))
* adds font-awesome icons in a couple places ([23b085f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/23b085f55642728db2694b4894ae9438d97dcf07))


### Bug Fixes

* **transistors.service:** fixes credits resetting on every sale ([bb78060](https://gitlab.com/HeinousTugboat/transistors-inc/commit/bb78060ea492ec3648faa5c0a6b218e4718f9cc9))
* **transistors.service:** fixes transistors not resetting at the beginning of each round ([28b7ba4](https://gitlab.com/HeinousTugboat/transistors-inc/commit/28b7ba4336cc251df4681abb6ad8d081198d9109))
* fixes favicon hrefs to work with gitlab ([dff9f2c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/dff9f2c58732d394a2303348910049fd38e01dbf))

### [0.0.1-alpha.1](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.0...v0.0.1-alpha.1) (2020-05-24)


### Features

* adds heavy styling and layout changes, initial theming pass! ([99450fe](https://gitlab.com/HeinousTugboat/transistors-inc/commit/99450fe539f9050d88c33ebf23d50adcc52414d2))
* **ai.service:** adds initial pass at AIs that modify your percentages, decoupled base rates ([cc16094](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cc16094c1dc6ef3ecd2db86dad53f60266a992f2))
* **transistors.service:** adds ability to upgrade length by selling transistors ([f5b0be8](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f5b0be8a7841b9bb5e5797328feea584316036b7))
* adds design and home components, basic routes ([7d534d2](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7d534d22ed6cb87194c2d3d9197cdc6850a76168))
* adds version to app component ([7d74815](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7d748159537202d8bde1c1e8a4aaa26c6cfbceab))

### [0.0.1-alpha.0](https://gitlab.com/HeinousTugboat/transistors-inc/compare/5d84a1cb9729b576093fd0c15af9e4be93d649d9...v0.0.1-alpha.0) (2020-05-23)


### Features

* **loop.service:** adds initial loop service and interface ([5d84a1c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5d84a1cb9729b576093fd0c15af9e4be93d649d9))
* **transistors.service:** adds initial transistors service ([500fcca](https://gitlab.com/HeinousTugboat/transistors-inc/commit/500fccae42ca1217a0671f3d694b8afff6e51a25))
