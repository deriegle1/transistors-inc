import { TestBed } from '@angular/core/testing';

import { SerializationService } from './serialization.service';

describe('SerializationService', () => {
  let service: SerializationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SerializationService);
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('saveState', () => {
    it('should save the state to local storage', () => {
      service.saveState({
        upgradeIds: ['r-1'],
      });

      expect(service.loadState()).toEqual({
        upgradeIds: ['r-1'],
      });
    });
  });

  describe('loadState', () => {
    it('should load the saved state', () => {
      expect(service.loadState()).toBeNull();

      service.saveState({
        upgradeIds: [],
      });

      expect(service.loadState()).toEqual({
        upgradeIds: [],
      });
    });
  });

  describe('updateState', () => {
    it('should update the existing state', () => {
      service.saveState({
        upgradeIds: ['r-1', 'r-2', 'r-3'],
      });

      service.updateState((curr) => curr);

      expect(service.loadState()).toEqual({
        upgradeIds: ['r-1', 'r-2', 'r-3'],
      });

      service.updateState((curr) => ({
        upgradeIds: [],
      }));

      expect(service.loadState()).toEqual({
        upgradeIds: [],
      });
    });

    it('should work with no local storage value', () => {
      expect(service.loadState()).toBeNull();

      service.updateState((curr) => ({
        ...curr,
          upgradeIds: ['r-1', 'r-2'],
      }));

      expect(service.loadState()).toEqual({
        upgradeIds: ['r-1', 'r-2'],
      });
    });
  });
});
