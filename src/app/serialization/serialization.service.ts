import { Injectable } from '@angular/core';

import { GameState } from './game-state.data';

type UpdateStateCallback = (s: GameState) => GameState;
// tslint:disable: no-console
@Injectable({
  providedIn: 'root',
})
export class SerializationService {
  private readonly storageKey = '___gameStorage';

  async saveState(state: GameState): Promise<boolean> {
    if (state) {
      localStorage.setItem(this.storageKey, JSON.stringify(state));
      console.log('[SERIALIZATION] saveState', JSON.stringify(state));
    }

    return true;
  }

  public updateState(cb: UpdateStateCallback): void {
    let newState: GameState;
    const storageState = this.loadState();

    if (storageState === null) {
      newState = cb({
        upgradeIds: [],
      });
    } else {
      newState = cb(storageState);
    }

    this.saveState(newState);
  }

  public loadState(): GameState | null {
    const state: unknown = localStorage.getItem(this.storageKey);

    console.log('[SERIALIZATION] loadState', state);

    if (typeof state === 'string') {
      try {
        const parsedState = JSON.parse(state);

        console.log('[SERIALIZATION] parsedState', parsedState);

        if (
          typeof parsedState === 'object' &&
          Array.isArray(parsedState.upgradeIds)
        ) {
          return parsedState as GameState;
        }
      } catch (err) {
        console.error('[SERIALIZATION] error parsing');
        return null;
      }
    }

    return null;
  }

  public resetState(): void {
    this.saveState({ upgradeIds: [] });
  }
}
