import { TestBed } from '@angular/core/testing';

import { TransistorsService } from './transistors.service';

describe('TransistorsService', () => {
  let service: TransistorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransistorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
