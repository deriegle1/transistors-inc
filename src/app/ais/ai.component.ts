import { Component, Input, OnInit } from '@angular/core';

import { assertNever } from '../utils';
import { Ai } from './ai';
import { AiModifierNames, AiModifierTypeRarities, AiTierNames } from './ai.data';
import { AiModifier, AiModifierRarity, AiModifierType, AiTier } from './ai.interface';

@Component({
  selector: 'app-ai',
  templateUrl: './ai.component.html',
  styleUrls: ['./ai.component.scss']
})
export class AiComponent implements OnInit {
  @Input() ai!: Ai;
  @Input() generating = false;

  tierName = AiTierNames;
  constructor() { }

  ngOnInit(): void {
  }

  get rarityClass(): string {
    switch (this.ai.tier) {
      case AiTier.NUM_TIERS:
      case AiTier.UNDERGRAD: return 'undergrad';
      case AiTier.GRAD: return 'grad';
      case AiTier.PROFESSOR: return 'professor';
      case AiTier.NOBEL: return 'nobel';
      case AiTier.BREAKTHROUGH: return 'breakthrough';
      default:
        return assertNever(this.ai.tier);
    }
  }

  modName(bonus: AiModifierType): string {
    return AiModifierNames[bonus];
  }

  lineClass(line: AiModifier): string {
    if (AiModifierTypeRarities[AiModifierRarity.SPECIAL].includes(line.type)) {
      return 'special';
    }

    return 'normal';
  }
}
