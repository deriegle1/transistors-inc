import { AiModifierData, AiModifierRarity, AiModifierType, AiTier } from './ai.interface';

export const AiTiers: AiTier[] = [
  AiTier.UNDERGRAD,
  AiTier.GRAD,
  AiTier.PROFESSOR,
  AiTier.NOBEL,
  AiTier.BREAKTHROUGH,
];

export const AiTierNames: { [key in AiTier]: string } = {
  [AiTier.UNDERGRAD]: 'Undergrad’s',
  [AiTier.GRAD]: 'Grad’s',
  [AiTier.PROFESSOR]: 'Professor’s',
  [AiTier.NOBEL]: 'Nobel’s',
  [AiTier.BREAKTHROUGH]: 'Breakthrough',
  [AiTier.NUM_TIERS]: '',
};

export const AiModifierRanges: { [key in AiModifierType]: AiModifierData } = {
  // Normal modifiers
  [AiModifierType.IMPROVE_CAP]: { min: 2, spread: 2 },
  [AiModifierType.IMPROVE_CAP_GROWTH]: { min: 3, spread: 6 },
  [AiModifierType.IMPROVE_COMPUTE]: { min: 5, spread: 5 },
  [AiModifierType.IMPROVE_CREDIT_EXCHANGE]: { min: 5, spread: 5 },

  // Special modifiers
  [AiModifierType.IMPROVE_REPLICATION]: { min: 1, spread: 1 },
  [AiModifierType.REDUCE_MASS]: { min: 1, spread: 2 },
  [AiModifierType.IMPROVE_CHIP_GAIN]: { min: 1, spread: 3 },
  [AiModifierType.ELEVATE_COMPUTE]: { min: 3, spread: 2 },
  [AiModifierType.ELEVATE_CREDIT_EXCHANGE]: { min: 3, spread: 2 },
};

export const AiModifierNames: { [key in AiModifierType]: string } = {
  // Normal modifiers
  [AiModifierType.IMPROVE_CAP]: 'Improve Cap',
  [AiModifierType.IMPROVE_CAP_GROWTH]: 'Improve Cap Growth',
  [AiModifierType.IMPROVE_COMPUTE]: 'Improve Compute Generation',
  [AiModifierType.IMPROVE_CREDIT_EXCHANGE]: 'Improve Credit Exchange',

  // Special modifiers
  [AiModifierType.IMPROVE_REPLICATION]: 'Improve Transistor Replication',
  [AiModifierType.REDUCE_MASS]: 'Reduce Transistor Mass',
  [AiModifierType.IMPROVE_CHIP_GAIN]: 'Improve Chip Gain',
  [AiModifierType.ELEVATE_COMPUTE]: 'Elevate Compute Generation',
  [AiModifierType.ELEVATE_CREDIT_EXCHANGE]: 'Elevate Credit Exchange'
};

export const AiModifierTypeRarities: { [key in AiModifierRarity]: AiModifierType[] } = {
  [AiModifierRarity.NORMAL]: [
    AiModifierType.IMPROVE_CAP,
    AiModifierType.IMPROVE_CAP_GROWTH,
    AiModifierType.IMPROVE_COMPUTE,
    AiModifierType.IMPROVE_CREDIT_EXCHANGE
  ],
  [AiModifierRarity.SPECIAL]: [
    AiModifierType.IMPROVE_REPLICATION,
    AiModifierType.REDUCE_MASS,
    AiModifierType.IMPROVE_CHIP_GAIN,
    AiModifierType.ELEVATE_COMPUTE,
    AiModifierType.ELEVATE_CREDIT_EXCHANGE
  ]
};
