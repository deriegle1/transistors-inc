import Decimal from 'break_infinity.js';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ai } from './ai';
import { AiComponent } from './ai.component';

describe('AiComponent', () => {
  let component: AiComponent;
  let fixture: ComponentFixture<AiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AiComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiComponent);
    component = fixture.componentInstance;
    component.ai = new Ai(new Decimal(0), new Decimal(0), 0, []);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
