import { AiTiers } from './ai.data';
import { AiTier } from './ai.interface';

export function findTier(n: number, f: number): AiTier {
  if (n < 0 || n > 1) { throw new RangeError(`choice ${n} is either below 0% or above 100%`); }
  if (f < 0 || f > 1) { throw new RangeError(`range modifier ${f} is either below 0% or above 100%`); }
  for (const tier of AiTiers) {
    if (n < 1 - ((1 - f) ** (tier + 1))) {
      return tier;
    }
  }
  return AiTier.BREAKTHROUGH;
}

