import { Component, OnInit } from '@angular/core';

import { OptionsService } from '../options.service';
import { AiModifierNames } from './ai.data';
import { AiModifierType } from './ai.interface';
import { AiService } from './ai.service';

@Component({
  selector: 'app-ais',
  templateUrl: './ais.component.html',
  styleUrls: ['./ais.component.scss']
})
export class AisComponent implements OnInit {
  public AiModifierNames = AiModifierNames;
  constructor(public ais: AiService, public options: OptionsService) { }

  ngOnInit(): void {
  }

  getModifierName(type: AiModifierType): string {
    return AiModifierNames[type];
  }

}
