import { Decimal } from '../../vendor';
import { Ai } from './ai';
import { AiTier } from './ai.interface';
import { findTier } from './ai.utils';

describe('Ai', () => {
  it('should create an instance', () => {
    expect(new Ai(new Decimal(Date.now()), new Decimal(1), 0, [])).toBeTruthy();
  });
});

describe('findTier', () => {
  it('should throw an error for n < 0', () => {
    expect(() => findTier(-0.01, 0.8)).toThrow();
  });
  it('should throw an error for n > 1.0', () => {
    expect(() => findTier(1.01, 0.8)).toThrow();
  });
  it('should throw an error for f < 0', () => {
    expect(() => findTier(0.5, -0.01)).toThrow();
  });
  it('should throw an error for f > 1.0', () => {
    expect(() => findTier(0.5, 1.01)).toThrow();
  });

  describe('f = 0.80', () => {
    it('should return UNDERGRAD for n = 0.00', () => {
      expect(findTier(0.79, 0.8)).toEqual(AiTier.UNDERGRAD);
    });

    it('should return UNDERGRAD for n = 0.79', () => {
      expect(findTier(0.79, 0.8)).toEqual(AiTier.UNDERGRAD);
    });

    it('should return GRAD for n = 0.80', () => {
      expect(findTier(0.80, 0.8)).toEqual(AiTier.GRAD);
    });

    it('should return GRAD for n = 0.95', () => {
      expect(findTier(0.95, 0.8)).toEqual(AiTier.GRAD);
    });

    it('should return PROFESSOR for n = 0.96', () => {
      expect(findTier(0.96, 0.8)).toEqual(AiTier.PROFESSOR);
    });

    it('should return PROFESSOR for n = 0.991', () => {
      expect(findTier(0.991, 0.8)).toEqual(AiTier.PROFESSOR);
    });

    it('should return NOBEL for n = 0.992', () => {
      expect(findTier(0.992, 0.8)).toEqual(AiTier.NOBEL);
    });

    it('should return NOBEL for n = 0.9983', () => {
      expect(findTier(0.9983, 0.8)).toEqual(AiTier.NOBEL);
    });

    it('should return BREAKTHROUGH for n = 0.9984', () => {
      expect(findTier(0.9984, 0.8)).toEqual(AiTier.BREAKTHROUGH);
    });

    it('should return BREAKTHROUGH for n = 0.9999', () => {
      expect(findTier(0.9999, 0.8)).toEqual(AiTier.BREAKTHROUGH);
    });
  });
});
