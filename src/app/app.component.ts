import { Component } from '@angular/core';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

import { environment } from '../environments/environment';
import { Decimal } from '../vendor';
import { AiService } from './ais/ai.service';
import { Currency as C } from './currency.interface';
import { CurrencyService } from './currency.service';
import { LogService } from './log/log.service';
import { LoopService } from './loop.service';
import { ProgressBarType } from './progress/progress.component';
import { MS_PER_SEC, TransistorsService, TransistorState } from './transistors.service';
import { BonusType } from './upgrades/upgrade.interface';
import { UpgradesService } from './upgrades/upgrades.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'transistor-inc';
  MS_PER_SEC = MS_PER_SEC;
  TransistorState = TransistorState;
  environment = environment;
  linkIcon = faExternalLinkAlt;
  roundTimerBar = ProgressBarType.ROUND_TIMER;
  Decimal = Decimal;
  ACC_TRANSISTOR = C.ACC_TRANSISTOR;
  TRANSISTOR = C.TRANSISTOR;
  COMPUTE = C.COMPUTE;
  CREDIT = C.CREDIT;
  REPLICATION = BonusType.R_MULT;
  CAP = BonusType.M_MULT;
  CAP_GROWTH = BonusType.T;

  // tslint:disable-next-line: max-line-length
  get durationString() { return `${this.transistors.roundDuration?.div(MS_PER_SEC)}/${this.transistors.roundLength.div(MS_PER_SEC)} seconds`; }

  get one() { return new Decimal(1); }
  get quarter() { return Decimal.floor(this.currency[C.ACC_TRANSISTOR].div(4)); }
  get half() { return Decimal.floor(this.currency[C.ACC_TRANSISTOR].div(2)); }
  get threeQuarters() { return Decimal.floor(this.currency[C.ACC_TRANSISTOR].mul(3).div(4)); }
  get allTransistors() { return Decimal.floor(this.currency[C.ACC_TRANSISTOR]); }

  sellOne(): void { this.transistors.sell(new Decimal(1)); }
  sellQuarter(): void { this.transistors.sell(this.quarter); }
  sellHalf(): void { this.transistors.sell(this.half); }
  sellThreeQuarter(): void { this.transistors.sell(this.threeQuarters); }
  sellAll(): void { this.transistors.sell(this.allTransistors); }

  creditAmount(n: Decimal = this.currency[this.ACC_TRANSISTOR]) {
    return this.transistors.baseCredits(n);
  }


  constructor(
    private log: LogService,
    public loop: LoopService,
    public transistors: TransistorsService,
    public currency: CurrencyService,
    public ai: AiService,
    public upgrades: UpgradesService,
  ) {
    log.add('System ready');
  }
}
