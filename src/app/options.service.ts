import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OptionsService {
  public format = true;
  public logLength = 15;
  constructor() { }
}
