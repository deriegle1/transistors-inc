import { Injectable } from '@angular/core';

import { Decimal, RNG, romDuoJrRandom } from '../vendor';

@Injectable({
  providedIn: 'root'
})
export class RandomService {
  private rand!: RNG;

  constructor() {
    this.init();
  }

  get seed(): number[] {
    return this.rand.seed;
  }

  reseed(seed?: [number, number]): void {
    this.rand = romDuoJrRandom(seed);
  }

  init(seed: number = Date.now()): void {
    this.rand = romDuoJrRandom();
    this.rand.init(seed);
  }

  next(): number { return this.rand(); }
  nextBig(): BigInt { return BigInt(this.rand()); }
  nextDec(): Decimal { return new Decimal(this.rand()); }
  range(n: number): number { return this.rand.range(n); }
}
