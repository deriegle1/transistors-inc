import { TestBed } from '@angular/core/testing';

import { Decimal } from '../vendor';
import { Currency } from './currency.interface';
import { CurrencyService } from './currency.service';

describe('CurrencyService', () => {
  let service: CurrencyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CurrencyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('setters and getters', () => {
    it('should update the value', () => {
      service[Currency.CREDIT] = new Decimal(20);
      expect(service[Currency.CREDIT]).toEqual(new Decimal(20));
      service[Currency.CREDIT] = new Decimal(40);
      expect(service[Currency.CREDIT]).toEqual(new Decimal(40));
    });

    it('should throw an error if set to a negative number', () => {
      expect(() => service[Currency.CREDIT] = new Decimal(-1)).toThrowError(/-1/);
    });
  });

  describe('set', () => {
    it('should work for a positive number', () => {
      service.set(Currency.CREDIT, new Decimal(5));
      expect(service[Currency.CREDIT]).toEqual(new Decimal(5));
    });

    it('should work for zero', () => {
      service.set(Currency.CREDIT, new Decimal(5));
      expect(service[Currency.CREDIT]).toEqual(new Decimal(5));
      service.set(Currency.CREDIT, new Decimal(0));
      expect(service[Currency.CREDIT]).toEqual(new Decimal(0));
    });

    it('should throw an error for a negative number', () => {
      expect(() => service.set(Currency.CREDIT, new Decimal(-1))).toThrowError(/-1/);
    });
  });

  describe('add', () => {
    beforeEach(() => {
      service.set(Currency.CREDIT, new Decimal(50));
    });

    it('should work for a postive number', () => {
      service.add(Currency.CREDIT, new Decimal(100));
      expect(service[Currency.CREDIT]).toEqual(new Decimal(150));
    });

    it('should throw an error for a negative number', () => {
      expect(() => service.add(Currency.CREDIT, new Decimal(-1))).toThrowError(/-1/);
    });

    it('should work with zero', () => {
      service.add(Currency.CREDIT, new Decimal(0));
      expect(service[Currency.CREDIT]).toEqual(new Decimal(50));
    });

    describe('with max parameter', () => {
      it('should work for a cap far above current values', () => {
        service.add(Currency.CREDIT, new Decimal(50), new Decimal(1000));
        expect(service[Currency.CREDIT]).toEqual(new Decimal(100));
      });

      it('should clamp to the max if provided', () => {
        service.add(Currency.CREDIT, new Decimal(50), new Decimal(75));
        expect(service[Currency.CREDIT]).toEqual(new Decimal(75));
      });

      it('should **lower** amount if clamped max less than current amount', () => {
        service.add(Currency.CREDIT, new Decimal(1), new Decimal(10));
        expect(service[Currency.CREDIT]).toEqual(new Decimal(10));
      });
    });
  });

  describe('subtract', () => {
    beforeEach(() => {
      service.set(Currency.CREDIT, new Decimal(50));
    });

    it('should work for a positive number', () => {
      service.subtract(Currency.CREDIT, new Decimal(15));
      expect(service[Currency.CREDIT]).toEqual(new Decimal(35));
    });

    it('should throw an error for a negative number', () => {
      expect(() => service.subtract(Currency.CREDIT, new Decimal(-1))).toThrowError(/-1/);
    });

    it('should throw an error if amount to subtract is greater than amount available', () => {
      expect(() => service.subtract(Currency.CREDIT, new Decimal(101))).toThrowError(/101/);
    });
  });

  describe('canAfford', () => {
    beforeEach(() => {
      service.set(Currency.CREDIT, new Decimal(50));
    });

    it('should return true if amount less than available', () => {
      expect(service.canAfford(Currency.CREDIT, new Decimal(35))).toEqual(true);
    });

    it('should return false if amount greater than available', () => {
      expect(service.canAfford(Currency.CREDIT, new Decimal(65))).toEqual(false);
    });

    it('should return true if amount exactly equal to available', () => {
      expect(service.canAfford(Currency.CREDIT, new Decimal(50))).toEqual(true);
    });
  });

  describe('multi-currency', () => {
    it('should independently track all 4 currencies', () => {
      service.set(Currency.CREDIT, new Decimal(50));
      service.set(Currency.COMPUTE, new Decimal(51));
      service.set(Currency.TRANSISTOR, new Decimal(52));
      service.set(Currency.ACC_TRANSISTOR, new Decimal(53));

      service.add(Currency.CREDIT, new Decimal(10));
      service.add(Currency.COMPUTE, new Decimal(11));
      service.add(Currency.TRANSISTOR, new Decimal(12));
      service.add(Currency.ACC_TRANSISTOR, new Decimal(13));

      expect(service[Currency.CREDIT]).toEqual(new Decimal(60));
      expect(service[Currency.COMPUTE]).toEqual(new Decimal(62));
      expect(service[Currency.TRANSISTOR]).toEqual(new Decimal(64));
      expect(service[Currency.ACC_TRANSISTOR]).toEqual(new Decimal(66));

      service.subtract(Currency.CREDIT, new Decimal(15));
      service.subtract(Currency.COMPUTE, new Decimal(16));
      service.subtract(Currency.TRANSISTOR, new Decimal(17));
      service.subtract(Currency.ACC_TRANSISTOR, new Decimal(18));

      expect(service[Currency.CREDIT]).toEqual(new Decimal(45));
      expect(service[Currency.COMPUTE]).toEqual(new Decimal(46));
      expect(service[Currency.TRANSISTOR]).toEqual(new Decimal(47));
      expect(service[Currency.ACC_TRANSISTOR]).toEqual(new Decimal(48));
    });
  });
});
