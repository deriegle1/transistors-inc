import { MarkdownModule } from 'ngx-markdown';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChangelogComponent } from './changelog/changelog.component';
import { DesignComponent } from './design/design.component';
import { HomeComponent } from './home/home.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LogComponent } from './log/log.component';
import { UpgradesComponent } from './upgrades/upgrades.component';
import { AisComponent } from './ais/ais.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DiagnosticsComponent } from './diagnostics/diagnostics.component';
import { ProgressComponent } from './progress/progress.component';
import { UnitsPipe } from './units.pipe';
import { OptionsComponent } from './options/options.component';
import { SliderComponent } from './slider/slider.component';
import { AiComponent } from './ais/ai.component';

@NgModule({
  declarations: [
    AppComponent,
    DesignComponent,
    HomeComponent,
    ChangelogComponent,
    LogComponent,
    UpgradesComponent,
    AisComponent,
    NotFoundComponent,
    DiagnosticsComponent,
    ProgressComponent,
    UnitsPipe,
    OptionsComponent,
    SliderComponent,
    AiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MarkdownModule.forRoot({ loader: HttpClient }),
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
