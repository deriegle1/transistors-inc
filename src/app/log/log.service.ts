import { Injectable } from '@angular/core';

import { LogItem, LogLevel } from './log.interface';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  history: LogItem[] = [];
  private nextId = 1;

  add(text: string, level: LogLevel = LogLevel.INFO): void {
    const logItem: LogItem = { text, id: `item-${this.nextId++}`, time: new Date(), level };
    this.history.push(logItem);
    // console.log(`Log: ${JSON.stringify(logItem)}`);
  }

  error(text: string): void {
    this.add(text, LogLevel.ERROR);
  }
}
