import { Component, OnInit } from '@angular/core';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

import { OptionsService } from '../options.service';
import { LogItem, LogLevel } from './log.interface';
import { LogService } from './log.service';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {
  errorIcon = faExclamationTriangle;
  errorLevel = LogLevel.ERROR;
  constructor(public log: LogService, public options: OptionsService) { }

  ngOnInit(): void {
  }

  item(i: number, item: LogItem): string { return item.id; }

}
