import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AisComponent } from './ais/ais.component';
import { ChangelogComponent } from './changelog/changelog.component';
import { DesignComponent } from './design/design.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { OptionsComponent } from './options/options.component';
import { UpgradesComponent } from './upgrades/upgrades.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'upgrades', component: UpgradesComponent },
  { path: 'ais', component: AisComponent },
  { path: 'design', component: DesignComponent },
  { path: 'changelog', component: ChangelogComponent },
  { path: 'options', component: OptionsComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
