import { Decimal } from '../../vendor';
import { Currency } from '../currency.interface';

export enum BonusType {
  R_ADD = 'r_add',
  R_MULT = 'r_mult',
  M_ADD = 'm_add',
  M_MULT = 'm_mult',
  T = 't_mult'
}

export interface Bonus {
  type: BonusType;
  amount: Decimal;
}

export interface Cost {
  currency: Currency;
  amount: Decimal;
}

export interface Upgrade {
  id: string;
  level: Decimal;
  name: string;
  cost: Cost;
  bonus: Bonus;
  repeating: boolean;
  generator?: (level: Decimal) => Upgrade;
  requirements: Upgrade[];
}
