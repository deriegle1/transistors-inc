import { Component, OnInit } from '@angular/core';

import { CurrencyService } from '../currency.service';
import { BonusTypeNames } from './upgrade.data';
import { BonusType } from './upgrade.interface';
import { UpgradesService } from './upgrades.service';

@Component({
  selector: 'app-upgrades',
  templateUrl: './upgrades.component.html',
  styleUrls: ['./upgrades.component.scss']
})
export class UpgradesComponent implements OnInit {

  constructor(public upgrades: UpgradesService, public currency: CurrencyService) { }

  ngOnInit(): void {
  }

  getModifierName(type: BonusType): string {
    return BonusTypeNames[type];
  }
}
