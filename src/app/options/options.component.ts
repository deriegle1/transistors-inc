import { Component, OnInit } from '@angular/core';

import { SerializationService } from '../serialization/serialization.service';
import { UpgradesService } from '../upgrades/upgrades.service';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {

  constructor(private serializer: SerializationService, private upgrades: UpgradesService) { }

  ngOnInit(): void {
  }

  reset() {
    this.serializer.resetState();
    // tslint:disable-next-line: no-string-literal
    this.upgrades['initializeSavedState']();
  }

}
