import { Component, OnInit } from '@angular/core';
import { faArrowAltCircleLeft, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.scss']
})
export class DesignComponent implements OnInit {
  externalIcon = faExternalLinkAlt;
  backIcon = faArrowAltCircleLeft;
  constructor() { }

  ngOnInit(): void {
  }

}
