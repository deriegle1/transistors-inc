export const enum Currency {
  CREDIT = 'credit',
  COMPUTE = 'compute',
  TRANSISTOR = 'transistor',
  ACC_TRANSISTOR = 'accumulated transistor'
}
