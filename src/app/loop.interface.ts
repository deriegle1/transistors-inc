import { Decimal } from '../vendor';

export interface Loop {
  interval: Decimal;
  time: Decimal;
  elapsedTime: Decimal;
  realTime: Decimal;
  tick: Decimal;
  debug: boolean;
  delay: Decimal;
  offset: Decimal;
}
